(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/* Author SnAtVB */
/* Тут мы собираем скрипт */

;(function() {

var app = {};
app.Models = require('./models.js');
app.View = require('./view.js');

app.Models(app);
app.ViewData = app.View(app);
// window.data.Person.create(['Михаил', 'Карелин', 'Дмитриевич', 'misha@ya.ru', '+7(800)436-11-77']);
// _data_.Person.remove(_data_.Person.get('first_name', "Михаил"));
// console.log(_data_.Person);
// window.app = app;
})();
},{"./models.js":9,"./view.js":10}],2:[function(require,module,exports){
/* Author SnAtVB */

/* 
    Создаем собственный шаблонизатор
*/

var Functions = require('./functions.js');

var BASETMPL = {
	render: function(html, data, ptn){
        ptn = ptn || /\{\%\s*[a-zA-Z0-9._/:-]+?\s*\%\}/g;
        if(!data) return html;
        // console.log(html);
        // console.log(data);
        var parts, matches, match, str, e, d; // объявляем переменные
        str = '';
        parts = html.split(ptn);
        matches = html.match(ptn);
        str += parts[0];
        if (!matches) return str; // Если нет наших переменных, то возвращаем строку
        
        for (var i = 0; i < matches.length; i++) {
            match = matches [i];
            e = match.replace(/^\{\%\s*|\s*\%\}$/g, ''); // чистим элемент
            d = Functions.getData(e, data);
            if(d!=undefined && d!=null) str += d;
            str += parts[i+1];
        }
        // str += parts[ matches.length ];
        return str;
    }
}; 

module.exports = BASETMPL;
},{"./functions.js":3}],3:[function(require,module,exports){
/* Author SnAtVB */

var Functions = {
    getData: function(data, when){
        var res, aname;
        aname = data.split('.');
        // console.log(data);
        // console.log(when);
        try{
            if(when[aname[0]] != undefined) res = when[aname[0]];
            for ( var i = 1; i < aname.length; i++ ){
                if(res[aname[i]]!=undefined)
                    res = res[aname[i]];
            }
            return res;
        } catch (err) {
            return false;
        }
    },
    forModelData: function(data){
        var res = [], obj = {};
        for (var i = 0; i<data.length; i++){
            obj[data[i].name] = data[i].value;
            if(data[i].name == 'id') continue;
            res.push(data[i].value);
        }
        return {arr: res, obj: obj};
    }
};

module.exports = Functions;
},{}],4:[function(require,module,exports){
/* Author SnAtVB */

/* 
    Создаем логгер, для вывода сообщений. Его можно всегда расширять и заменять.
*/

function LoggerSN () {
    this.init();
}

LoggerSN.prototype = {
    init: function(){
        this._name_ = "DebuggerSN";
    },
    log: function(message){
        if(message == undefined) return;
        console.log(message);
    },
    error: function(message){
        if(message == undefined) return;
        console.error(message);
    },
    warn: function(message){
        if(message == undefined) return;
        console.warn(message);
    },
    info: function(message){
        if(message == undefined) return;
        console.info(message);
    },
    success: function(message){
        if(message == undefined) return;
        console.info(message);
    }
};

module.exports = new LoggerSN();
},{}],5:[function(require,module,exports){
/* Author SnAtVB */

/* 
    Создаем представления, для хранения их в LocalStorage
*/

var Logger = require('./logger.js');

function BaseModel (name, model) {
    this.model = model || null;
    this.objects = [];
    this._id_ = 1; // Начальный ID
    this.name = name;
    return this.init();
}

BaseModel.prototype = {
    init: function(){
        if (!this.model || !this.name) return null;
        if(!this.model.id) this.model.unshift('id');
        this.loadData();
        return this;
    },
    create: function(obj, f){ // Создание элемента
        var kk, now_objects = this.genItem(obj);
        for(var k in now_objects){
            kk = k.split('__');
            console.log(kk[1]);
        }
        this.objects.push(now_objects);
        this.updateData();
        if(f) f(this.get('id', this._id_-1));
        return obj;
    },
    genItem: function (obj){ // Генерируем элемент
        var res = {};
        for ( var i = 0; i < this.model.length; i++ ){
            if ( i == 0 || i === '_meta' ) {
                res.id = this._id_;
                continue;
            }
            res[this.model[i]] = obj[i-1];
        }
        res._meta = { // Добавляем мета объект, в котором будут храниться дополнительные данные
            model_name: this.name,
            index: this.objects.length
        };
        this._id_++;
        return res;
    },
    get: function(when, what){ // Выбираем первое вхождение
        for (var i = 0; i < this.objects.length; i++){
            if (this.objects[i][when] == what) {
                this.objects[i]._meta.nowindex = i;
                return this.objects[i];
            };
        }
        return false;
    },
    update: function(obj, data, f){ // Обновляем данные
        var i;
        i = 0;
        for ( var k in obj ){
            if ( k === 'id' || k === '_meta' ) {
                continue;
            }
            obj[k] = data[i];
            i++;
        }
        // obj._meta = { // Добавляем мета объект, в котором будут храниться дополнительные данные
        //     model_name: this.name,
        //     index: this.objects.length
        // };
        this.objects[obj._meta.index] = obj;
        this.updateData();
        if(f) f({object:obj, arr:data});
        return {object:obj, arr:data};
    },
    remove: function(obj, f){ // Удаление объекта
        try{
            this.objects.splice(obj._meta.nowindex, 1);
            this.updateData();
            if(f) f (obj);
        } catch (err) {
            Logger.error('Ошибка удаления');
        }
        
        return this.objects;
    },
    updateData: function(){ // Обновление данный в localStorage
        localStorage[this.name] = JSON.stringify({
            objects: this.objects, 
            _id_ : this._id_ 
        });
    },
    loadData: function(){ // Загрузка данных из localStorage
        if(localStorage[this.name]) {
            var res = {};
            try {
                res = JSON.parse(localStorage[this.name]);
                this.objects = res.objects;
                this._id_ = res._id_;
            } catch (err) {
                Logger.error(err);
                this.clearData();
            }
        }
        return res;
    },
    clearData: function(){ // Полное очищение данных
        delete localStorage[this.name];
        this.objects = [];
        return true;
    }
};

module.exports = {
    BaseModel: BaseModel
};
},{"./logger.js":4}],6:[function(require,module,exports){
/* Author SnAtVB */

var Functions = require('../functions.js');
var Sender = require('./send.js');
var BT = require('../base_templater.js');
var Logger = require('../logger.js');

var Forms = function(selector, stgs){
    this.selector = selector;
    this.stgs = stgs || {};
    return this.init();
};

Forms.prototype = {
    init: function(){
        if(!this.selector) return false;
        this.forms = $(this.selector);
        // console.log(this.forms);
        // this.getStgs();
        this.sender = new Sender();
        this.setListens();
        return this;
    },
    setListens: function(){
        for(var i=0; i < this.forms.length; i++)
            this.addListen(this.forms.eq(i));
    },
    addListen: function(form, f){
        var self = this;
        form.on('submit', function(event){
            event.preventDefault();
            self.send($(this), f);
        }); 
    },
    send: function(form, f){
        var self = this;
        var data = Functions.forModelData(form.serializeArray());
        var url = form.attr('action');
        var btn = $(':submit', form);
        btn.button('loading');
        if (!f) f = function(){
            var model, tmpl, res, what, type, id, thisobj, temp;
            what = form.data('data');
            tmpl = form.data('tmpl-index');
            tmpl = self.getTmpl(tmpl);
            type = form.data('type');
            if(what) model = Functions.getData(what, _data_);
            if(!model) return btn.button('reset');
            if(type == 'create') model.create(data.arr, function(obj){
                if(!tmpl) return true;
                res = $(BT.render(tmpl[0].outerHTML, obj));
                tmpl.before(res);
                res.show();
                btn.button('reset');
                form.hide();
            });
            if(type == 'update') {
                if(data.obj.id == undefined) return Logger.error('ID не найден'); 
                id = parseInt(data.obj.id);
                thisobj = model.get("id", id);
                model.update(thisobj, data.arr, function(obj){
                    if(!tmpl) return true;
                    res = $(BT.render(tmpl[0].outerHTML, obj.object));
                    temp = $('#row-' + obj.object._meta.model_name + '-' + obj.object._meta.index);
                    temp.before(res);
                    res.show();
                    temp.remove();
                    btn.button('reset');
                    form.hide();
                });
            }
        }
        this.sender.send(data).done(function(){
            return f();
        });
    },
    getTmpl: function(index){
        var res
        try {
            res = this.stgs.tmpl.blocks.eq(index);
        } catch (err) {
            return res;
        }
        return res;
    }
};

module.exports = Forms;
},{"../base_templater.js":2,"../functions.js":3,"../logger.js":4,"./send.js":7}],7:[function(require,module,exports){
/* Author SnAtVB */

var Sender = function(stgs){
    this.stgs = stgs || {};
    return this.init();
};

Sender.prototype = {
    init: function(){
        this.deferred = $.Deferred();
        this.getStgs();
    },
    getStgs: function(){
        this.stgs = {
            url: this.stgs.url || ''
        };
    },
    send: function(data){
        var self = this;
        setTimeout(function() { self.deferred.resolve(); }, 1500);
        return self.deferred.promise();
    }
};

module.exports = Sender;
},{}],8:[function(require,module,exports){
/* Author SnAtVB */

/* 
    Создаем собственный шаблонизатор
*/

var Functions = require('./functions.js');
var BT = require('./base_templater.js');

var TemplateSN = function(jq, stgs){
    this._name_ = 'TemplateSN';
    this.jq = jq;
    this.html = jq.html();
    this.stgs = stgs || {};
    return this.init();
};

TemplateSN.prototype = {
    init: function(){
        if (!this.html) return false;
        this.getStgs();
        this.blocks = this.jq.find('.' + this.stgs.blocks); // Собираем блоки
        return this;
    },
    getStgs: function(){
        this.stgs = {
            ptn: this.stgs.ptn || /\{\%\s*[a-zA-Z0-9._/:-]+?\s*\%\}/g,
            blocks: this.stgs.blocks || 'templater-snjs',
            clock: this.stgs.clock || 'sn-clock'
        };
    },
    render: function(html, data){
        return BT.render(html, data, this.stgs.ptn);
    },
    start: function(f){
        var self, res, jq, parent, data;
        self = this;
        res = '';
        this.blocks.each(function( index ){
            jq = $(this);
            // self.tmpls[index] = jq;
            // console.log(self.blocks.eq(index).after(index));
            jq.removeClass(self.stgs.clock);
            data = self.getData(jq.attr('sn-repeat'));
            if(data){
                for(var i = 0; i < data.length; i++) {
                    res += self.render(this.outerHTML, data[i]);
                }
            } else res += this.outerHTML;
            jq.before(res);
            jq.hide();
            jq.attr('data-index', index);
            self.addParentAttr(jq, index);
            if(f) f(jq, index);
            res = '';
        });
    },
    addParentAttr: function(jq, index){
        var data = {};
        data.parent = jq.data('parent');
        data.parent = jq.parents(data.parent);
        if(data.parent) {
            data.parent.attr('data-tmpl-index', index);
        }
        data.target = jq.data('target');
        data.target = data.target.replace(' ', '');
        data.target = data.target.split(',');
        var now;
        for(var i = 0; i<data.target.length; i++){
            now = $(data.target[i]);
            now.attr('data-tmpl-index', index);
        }
    },
    // update: function(){
    //     var jq, blocks, self = this;
    //     console.log(this.blocks);
    //     blocks = this.jq.find('.' + this.stgs.blocks); // Собираем блоки
    //     blocks.each(function( index ){
    //         jq = $(this);
    //         jq.addClass(self.stgs.clock);
    //         console.log(self.blocks.eq(index));
    //         jq.remove();
    //     });
    //     // this.start();
    // },
    getData: function(data){
        return Functions.getData(data, _data_);
    }
};

module.exports = TemplateSN;
},{"./base_templater.js":2,"./functions.js":3}],9:[function(require,module,exports){
/* Author SnAtVB */

var Model = require('./core/model.js');

module.exports = function(app){
    var Person;
    window._data_ = {};
    
    // Создаем модель персоны
    Person = new Model.BaseModel('person', ['first_name', 'last_name', 'middle_name', 'email', 'phone_number']);
    User = new Model.BaseModel('user', ['nickname', 'department__id', 'person__id', 'position__id', 'super_user']);
    Position = new Model.BaseModel('position', ['position_name', 'salary']);
    Department = new Model.BaseModel('department', ['department_name', 'company__id']);
    Company = new Model.BaseModel('company', ['company_name', 'description', 'logo']);
    // Person.create(['Иван', 'Иванов', 'Иванович', 'ivanov@ya.ru', '+7(800)444-55-55']);
    // Person.create(['Алексей', 'Реков', 'Иванович', 'lex@ya.ru', '+7(800)444-55-55']);
    // Person.create(['Кирилл', 'Нартов', 'Юрьевич', 'kira@ya.ru', '+7(800)444-55-55']);
    // Position.create(['Front-end', 40000]);
    // Person.clearData();
    // Person.remove(Person.get('first_name', "3"));
    // console.log(Person.objects);
    // console.log(Person.get('first_name', "Алексей"));
    // Person.update(Person.get('id', 2), ['Иван', 'Иванов', 'Иванович', 'ivanov@ya.ru', '+7(800)444-55-55']);
    window._data_.Person = Person;
    window._data_.User = User;
    window._data_.Position = Position;
    window._data_.Department = Department;
    window._data_.Company = Company;
    // Person.clearData();
};
},{"./core/model.js":5}],10:[function(require,module,exports){
/* Author SnAtVB */

var Template = require('./core/template.js');
var BT = require('./core/base_templater.js');
var Logger = require('./core/logger.js');
var Functions = require('./core/functions.js');
var Senders = {};
Senders.From = require('./core/senders/form.js');

module.exports = function(app){
    var templates = {};
    var b = $('.sn-scope');
    var tmpl = new Template(b);
    // var t = tmpl.render({id : '3', first_name: 'Name'});
    tmpl.start();
    // tmpl.update();
    // console.log(tmpl.tmpls[0]);
    // b.html(t);
    // console.log(app);

    var forms = new Senders.From('.sn-ajax_sender', {tmpl:tmpl, type:'create'}); // Инициализируем форму для создания

    // Добавляю фунцкии в объект для событий дабы не колдовать с добавлением прослушек событий
    window.Events = {};
    window.Events.model = {
        getForUpdate: function(event, on){
            PED(event);
            var jq, elems, e, data, temp_data, modeldata, tmpldata;
            jq = $(on);
            modeldata = jq.data('data');
            tmpldata = jq.data('tmpl-up');
            if (!modeldata || !tmpldata) return;
            elems = jq.find('[data-name-sn]');
            data = {};
            elems.each(function(index){
                e = $(this);
                temp_data = e.data('name-sn');
                if(temp_data != '' && temp_data != undefined) data[temp_data] = e.text();
            });
            // console.log(data);
            if(!templates[tmpldata]){
                templates[tmpldata] = { jq: $(tmpldata) };
                templates[tmpldata].jq.tmpl = templates[tmpldata].jq.html();
            }
            tmpldata = templates[tmpldata].jq;
            if(tmpldata.length == 0) return;
            // console.log(tmpldata.tmpl);
            if(!tmpldata.tmpl) tmpldata.tmpl = tmpldata.html();
            data.modelname = modeldata;
            tmpldata.html(BT.render(tmpldata.tmpl, data));
            // tmpldata = $(tmpldata).removeAttr('id');
            tmpldata.show();
        },
        deleteModelObject: function(event, on){
            PED(event);

            var modelname, model, obj, jq, id, parent;
            jq = $(on);
            modelname = jq.data('modelname');
            if(!modelname) return Logger.error('Модель не найдена');
            model = Functions.getData(modelname, _data_);
            id = jq.data('id');
            obj = model.get('id', id);
            if(!obj) return Logger.error('Объект id: ' + id + ' не найден');
            parent = jq.data('parent');
            if(parent) parent = jq.parents(parent);
            model.remove(obj, function(r_obj){
                $('#row-' + r_obj._meta.model_name + '-' + r_obj._meta.index).remove();
                Logger.success('Успешно удалено');
                if(parent) parent.hide();
            });
        }
    };

    var opener = function(elem){ // Простейшая функция свитчера show и hide
        var target, targets, now, jq = $(elem);
        target = jq.data('target');
        target = target.replace(' ', '');
        targets = target.split(',');
        if (targets.length == 0) return false;
        for(var i = 0; i<targets.length; i++){
            now = $(targets[i]);
            if(!now.is(':visible')) now.show();
            else now.hide();
        }
        return true;
    };
    $('.opener-sn').on('click', function(event){
        PED(event);

        opener(this);
    });

    var bodyhide = function(selection){
        var unit = $(selection);
        var body = $('body');
        body.on('click', function(event){
            unit.hide();
        });
        unit.on('click', function(event){
            event = event || window.event;
            if (event.stopPropagation) {
                event.stopPropagation()
            } else {
                event.cancelBubble = true;
            }
        });
    }
    bodyhide('.bodyhide');

    var PED = function(event){
        event = event || window.event;
        event.preventDefault();
        if (event.stopPropagation) {
            event.stopPropagation()
        } else {
            event.cancelBubble = true;
        }
    }

    return {tmpl: tmpl, forms: forms}; // Возвращаем данные для app.ViewData
};
},{"./core/base_templater.js":2,"./core/functions.js":3,"./core/logger.js":4,"./core/senders/form.js":6,"./core/template.js":8}]},{},[1]);

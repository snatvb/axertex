/* Author SnAtVB */

/* 
    Создаем собственный шаблонизатор
*/

var Functions = require('./functions.js');

var BASETMPL = {
	render: function(html, data, ptn){
        ptn = ptn || /\{\%\s*[a-zA-Z0-9._/:-]+?\s*\%\}/g;
        if(!data) return html;
        // console.log(html);
        // console.log(data);
        var parts, matches, match, str, e, d; // объявляем переменные
        str = '';
        parts = html.split(ptn);
        matches = html.match(ptn);
        str += parts[0];
        if (!matches) return str; // Если нет наших переменных, то возвращаем строку
        
        for (var i = 0; i < matches.length; i++) {
            match = matches [i];
            e = match.replace(/^\{\%\s*|\s*\%\}$/g, ''); // чистим элемент
            d = Functions.getData(e, data);
            if(d!=undefined && d!=null) str += d;
            str += parts[i+1];
        }
        // str += parts[ matches.length ];
        return str;
    }
}; 

module.exports = BASETMPL;
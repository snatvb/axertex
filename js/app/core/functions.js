/* Author SnAtVB */

var Functions = {
    getData: function(data, when){
        var res, aname;
        aname = data.split('.');
        // console.log(data);
        // console.log(when);
        try{
            if(when[aname[0]] != undefined) res = when[aname[0]];
            for ( var i = 1; i < aname.length; i++ ){
                if(res[aname[i]]!=undefined)
                    res = res[aname[i]];
            }
            return res;
        } catch (err) {
            return false;
        }
    },
    forModelData: function(data){
        var res = [], obj = {};
        for (var i = 0; i<data.length; i++){
            obj[data[i].name] = data[i].value;
            if(data[i].name == 'id') continue;
            res.push(data[i].value);
        }
        return {arr: res, obj: obj};
    }
};

module.exports = Functions;
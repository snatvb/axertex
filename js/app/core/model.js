/* Author SnAtVB */

/* 
    Создаем представления, для хранения их в LocalStorage
*/

var Logger = require('./logger.js');

function BaseModel (name, model) {
    this.model = model || null;
    this.objects = [];
    this._id_ = 1; // Начальный ID
    this.name = name;
    return this.init();
}

BaseModel.prototype = {
    init: function(){
        if (!this.model || !this.name) return null;
        if(!this.model.id) this.model.unshift('id');
        this.loadData();
        return this;
    },
    create: function(obj, f){ // Создание элемента
        var kk, now_objects = this.genItem(obj);
        for(var k in now_objects){
            kk = k.split('__');
            console.log(kk[1]);
        }
        this.objects.push(now_objects);
        this.updateData();
        if(f) f(this.get('id', this._id_-1));
        return obj;
    },
    genItem: function (obj){ // Генерируем элемент
        var res = {};
        for ( var i = 0; i < this.model.length; i++ ){
            if ( i == 0 || i === '_meta' ) {
                res.id = this._id_;
                continue;
            }
            res[this.model[i]] = obj[i-1];
        }
        res._meta = { // Добавляем мета объект, в котором будут храниться дополнительные данные
            model_name: this.name,
            index: this.objects.length
        };
        this._id_++;
        return res;
    },
    get: function(when, what){ // Выбираем первое вхождение
        for (var i = 0; i < this.objects.length; i++){
            if (this.objects[i][when] == what) {
                this.objects[i]._meta.nowindex = i;
                return this.objects[i];
            };
        }
        return false;
    },
    update: function(obj, data, f){ // Обновляем данные
        var i;
        i = 0;
        for ( var k in obj ){
            if ( k === 'id' || k === '_meta' ) {
                continue;
            }
            obj[k] = data[i];
            i++;
        }
        // obj._meta = { // Добавляем мета объект, в котором будут храниться дополнительные данные
        //     model_name: this.name,
        //     index: this.objects.length
        // };
        this.objects[obj._meta.index] = obj;
        this.updateData();
        if(f) f({object:obj, arr:data});
        return {object:obj, arr:data};
    },
    remove: function(obj, f){ // Удаление объекта
        try{
            this.objects.splice(obj._meta.nowindex, 1);
            this.updateData();
            if(f) f (obj);
        } catch (err) {
            Logger.error('Ошибка удаления');
        }
        
        return this.objects;
    },
    updateData: function(){ // Обновление данный в localStorage
        localStorage[this.name] = JSON.stringify({
            objects: this.objects, 
            _id_ : this._id_ 
        });
    },
    loadData: function(){ // Загрузка данных из localStorage
        if(localStorage[this.name]) {
            var res = {};
            try {
                res = JSON.parse(localStorage[this.name]);
                this.objects = res.objects;
                this._id_ = res._id_;
            } catch (err) {
                Logger.error(err);
                this.clearData();
            }
        }
        return res;
    },
    clearData: function(){ // Полное очищение данных
        delete localStorage[this.name];
        this.objects = [];
        return true;
    }
};

module.exports = {
    BaseModel: BaseModel
};
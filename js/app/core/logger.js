/* Author SnAtVB */

/* 
    Создаем логгер, для вывода сообщений. Его можно всегда расширять и заменять.
*/

function LoggerSN () {
    this.init();
}

LoggerSN.prototype = {
    init: function(){
        this._name_ = "DebuggerSN";
    },
    log: function(message){
        if(message == undefined) return;
        console.log(message);
    },
    error: function(message){
        if(message == undefined) return;
        console.error(message);
    },
    warn: function(message){
        if(message == undefined) return;
        console.warn(message);
    },
    info: function(message){
        if(message == undefined) return;
        console.info(message);
    },
    success: function(message){
        if(message == undefined) return;
        console.info(message);
    }
};

module.exports = new LoggerSN();
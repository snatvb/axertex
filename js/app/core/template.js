/* Author SnAtVB */

/* 
    Создаем собственный шаблонизатор
*/

var Functions = require('./functions.js');
var BT = require('./base_templater.js');

var TemplateSN = function(jq, stgs){
    this._name_ = 'TemplateSN';
    this.jq = jq;
    this.html = jq.html();
    this.stgs = stgs || {};
    return this.init();
};

TemplateSN.prototype = {
    init: function(){
        if (!this.html) return false;
        this.getStgs();
        this.blocks = this.jq.find('.' + this.stgs.blocks); // Собираем блоки
        return this;
    },
    getStgs: function(){
        this.stgs = {
            ptn: this.stgs.ptn || /\{\%\s*[a-zA-Z0-9._/:-]+?\s*\%\}/g,
            blocks: this.stgs.blocks || 'templater-snjs',
            clock: this.stgs.clock || 'sn-clock'
        };
    },
    render: function(html, data){
        return BT.render(html, data, this.stgs.ptn);
    },
    start: function(f){
        var self, res, jq, parent, data;
        self = this;
        res = '';
        this.blocks.each(function( index ){
            jq = $(this);
            // self.tmpls[index] = jq;
            // console.log(self.blocks.eq(index).after(index));
            jq.removeClass(self.stgs.clock);
            data = self.getData(jq.attr('sn-repeat'));
            if(data){
                for(var i = 0; i < data.length; i++) {
                    res += self.render(this.outerHTML, data[i]);
                }
            } else res += this.outerHTML;
            jq.before(res);
            jq.hide();
            jq.attr('data-index', index);
            self.addParentAttr(jq, index);
            if(f) f(jq, index);
            res = '';
        });
    },
    addParentAttr: function(jq, index){
        var data = {};
        data.parent = jq.data('parent');
        data.parent = jq.parents(data.parent);
        if(data.parent) {
            data.parent.attr('data-tmpl-index', index);
        }
        data.target = jq.data('target');
        data.target = data.target.replace(' ', '');
        data.target = data.target.split(',');
        var now;
        for(var i = 0; i<data.target.length; i++){
            now = $(data.target[i]);
            now.attr('data-tmpl-index', index);
        }
    },
    // update: function(){
    //     var jq, blocks, self = this;
    //     console.log(this.blocks);
    //     blocks = this.jq.find('.' + this.stgs.blocks); // Собираем блоки
    //     blocks.each(function( index ){
    //         jq = $(this);
    //         jq.addClass(self.stgs.clock);
    //         console.log(self.blocks.eq(index));
    //         jq.remove();
    //     });
    //     // this.start();
    // },
    getData: function(data){
        return Functions.getData(data, _data_);
    }
};

module.exports = TemplateSN;
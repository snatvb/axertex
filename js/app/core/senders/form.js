/* Author SnAtVB */

var Functions = require('../functions.js');
var Sender = require('./send.js');
var BT = require('../base_templater.js');
var Logger = require('../logger.js');

var Forms = function(selector, stgs){
    this.selector = selector;
    this.stgs = stgs || {};
    return this.init();
};

Forms.prototype = {
    init: function(){
        if(!this.selector) return false;
        this.forms = $(this.selector);
        // console.log(this.forms);
        // this.getStgs();
        this.sender = new Sender();
        this.setListens();
        return this;
    },
    setListens: function(){
        for(var i=0; i < this.forms.length; i++)
            this.addListen(this.forms.eq(i));
    },
    addListen: function(form, f){
        var self = this;
        form.on('submit', function(event){
            event.preventDefault();
            self.send($(this), f);
        }); 
    },
    send: function(form, f){
        var self = this;
        var data = Functions.forModelData(form.serializeArray());
        var url = form.attr('action');
        var btn = $(':submit', form);
        btn.button('loading');
        if (!f) f = function(){
            var model, tmpl, res, what, type, id, thisobj, temp;
            what = form.data('data');
            tmpl = form.data('tmpl-index');
            tmpl = self.getTmpl(tmpl);
            type = form.data('type');
            if(what) model = Functions.getData(what, _data_);
            if(!model) return btn.button('reset');
            if(type == 'create') model.create(data.arr, function(obj){
                if(!tmpl) return true;
                res = $(BT.render(tmpl[0].outerHTML, obj));
                tmpl.before(res);
                res.show();
                btn.button('reset');
                form.hide();
            });
            if(type == 'update') {
                if(data.obj.id == undefined) return Logger.error('ID не найден'); 
                id = parseInt(data.obj.id);
                thisobj = model.get("id", id);
                model.update(thisobj, data.arr, function(obj){
                    if(!tmpl) return true;
                    res = $(BT.render(tmpl[0].outerHTML, obj.object));
                    temp = $('#row-' + obj.object._meta.model_name + '-' + obj.object._meta.index);
                    temp.before(res);
                    res.show();
                    temp.remove();
                    btn.button('reset');
                    form.hide();
                });
            }
        }
        this.sender.send(data).done(function(){
            return f();
        });
    },
    getTmpl: function(index){
        var res
        try {
            res = this.stgs.tmpl.blocks.eq(index);
        } catch (err) {
            return res;
        }
        return res;
    }
};

module.exports = Forms;
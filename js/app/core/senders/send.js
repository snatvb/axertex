/* Author SnAtVB */

var Sender = function(stgs){
    this.stgs = stgs || {};
    return this.init();
};

Sender.prototype = {
    init: function(){
        this.deferred = $.Deferred();
        this.getStgs();
    },
    getStgs: function(){
        this.stgs = {
            url: this.stgs.url || ''
        };
    },
    send: function(data){
        var self = this;
        setTimeout(function() { self.deferred.resolve(); }, 1500);
        return self.deferred.promise();
    }
};

module.exports = Sender;
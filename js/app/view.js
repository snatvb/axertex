/* Author SnAtVB */

var Template = require('./core/template.js');
var BT = require('./core/base_templater.js');
var Logger = require('./core/logger.js');
var Functions = require('./core/functions.js');
var Senders = {};
Senders.From = require('./core/senders/form.js');

module.exports = function(app){
    var templates = {};
    var b = $('.sn-scope');
    var tmpl = new Template(b);
    // var t = tmpl.render({id : '3', first_name: 'Name'});
    tmpl.start();
    // tmpl.update();
    // console.log(tmpl.tmpls[0]);
    // b.html(t);
    // console.log(app);

    var forms = new Senders.From('.sn-ajax_sender', {tmpl:tmpl, type:'create'}); // Инициализируем форму для создания

    // Добавляю фунцкии в объект для событий дабы не колдовать с добавлением прослушек событий
    window.Events = {};
    window.Events.model = {
        getForUpdate: function(event, on){
            PED(event);
            var jq, elems, e, data, temp_data, modeldata, tmpldata;
            jq = $(on);
            modeldata = jq.data('data');
            tmpldata = jq.data('tmpl-up');
            if (!modeldata || !tmpldata) return;
            elems = jq.find('[data-name-sn]');
            data = {};
            elems.each(function(index){
                e = $(this);
                temp_data = e.data('name-sn');
                if(temp_data != '' && temp_data != undefined) data[temp_data] = e.text();
            });
            // console.log(data);
            if(!templates[tmpldata]){
                templates[tmpldata] = { jq: $(tmpldata) };
                templates[tmpldata].jq.tmpl = templates[tmpldata].jq.html();
            }
            tmpldata = templates[tmpldata].jq;
            if(tmpldata.length == 0) return;
            // console.log(tmpldata.tmpl);
            if(!tmpldata.tmpl) tmpldata.tmpl = tmpldata.html();
            data.modelname = modeldata;
            tmpldata.html(BT.render(tmpldata.tmpl, data));
            // tmpldata = $(tmpldata).removeAttr('id');
            tmpldata.show();
        },
        deleteModelObject: function(event, on){
            PED(event);

            var modelname, model, obj, jq, id, parent;
            jq = $(on);
            modelname = jq.data('modelname');
            if(!modelname) return Logger.error('Модель не найдена');
            model = Functions.getData(modelname, _data_);
            id = jq.data('id');
            obj = model.get('id', id);
            if(!obj) return Logger.error('Объект id: ' + id + ' не найден');
            parent = jq.data('parent');
            if(parent) parent = jq.parents(parent);
            model.remove(obj, function(r_obj){
                $('#row-' + r_obj._meta.model_name + '-' + r_obj._meta.index).remove();
                Logger.success('Успешно удалено');
                if(parent) parent.hide();
            });
        }
    };

    var opener = function(elem){ // Простейшая функция свитчера show и hide
        var target, targets, now, jq = $(elem);
        target = jq.data('target');
        target = target.replace(' ', '');
        targets = target.split(',');
        if (targets.length == 0) return false;
        for(var i = 0; i<targets.length; i++){
            now = $(targets[i]);
            if(!now.is(':visible')) now.show();
            else now.hide();
        }
        return true;
    };
    $('.opener-sn').on('click', function(event){
        PED(event);

        opener(this);
    });

    var bodyhide = function(selection){
        var unit = $(selection);
        var body = $('body');
        body.on('click', function(event){
            unit.hide();
        });
        unit.on('click', function(event){
            event = event || window.event;
            if (event.stopPropagation) {
                event.stopPropagation()
            } else {
                event.cancelBubble = true;
            }
        });
    }
    bodyhide('.bodyhide');

    var PED = function(event){
        event = event || window.event;
        event.preventDefault();
        if (event.stopPropagation) {
            event.stopPropagation()
        } else {
            event.cancelBubble = true;
        }
    }

    return {tmpl: tmpl, forms: forms}; // Возвращаем данные для app.ViewData
};
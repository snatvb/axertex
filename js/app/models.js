/* Author SnAtVB */

var Model = require('./core/model.js');

module.exports = function(app){
    var Person;
    window._data_ = {};
    
    // Создаем модель персоны
    Person = new Model.BaseModel('person', ['first_name', 'last_name', 'middle_name', 'email', 'phone_number']);
    User = new Model.BaseModel('user', ['nickname', 'department__id', 'person__id', 'position__id', 'super_user']);
    Position = new Model.BaseModel('position', ['position_name', 'salary']);
    Department = new Model.BaseModel('department', ['department_name', 'company__id']);
    Company = new Model.BaseModel('company', ['company_name', 'description', 'logo']);
    // Person.create(['Иван', 'Иванов', 'Иванович', 'ivanov@ya.ru', '+7(800)444-55-55']);
    // Person.create(['Алексей', 'Реков', 'Иванович', 'lex@ya.ru', '+7(800)444-55-55']);
    // Person.create(['Кирилл', 'Нартов', 'Юрьевич', 'kira@ya.ru', '+7(800)444-55-55']);
    // Position.create(['Front-end', 40000]);
    // Person.clearData();
    // Person.remove(Person.get('first_name', "3"));
    // console.log(Person.objects);
    // console.log(Person.get('first_name', "Алексей"));
    // Person.update(Person.get('id', 2), ['Иван', 'Иванов', 'Иванович', 'ivanov@ya.ru', '+7(800)444-55-55']);
    window._data_.Person = Person;
    window._data_.User = User;
    window._data_.Position = Position;
    window._data_.Department = Department;
    window._data_.Company = Company;
    // Person.clearData();
};
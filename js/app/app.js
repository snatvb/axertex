/* Author SnAtVB */
/* Тут мы собираем скрипт */

;(function() {

var app = {};
app.Models = require('./models.js');
app.View = require('./view.js');

app.Models(app);
app.ViewData = app.View(app);
// window.data.Person.create(['Михаил', 'Карелин', 'Дмитриевич', 'misha@ya.ru', '+7(800)436-11-77']);
// _data_.Person.remove(_data_.Person.get('first_name', "Михаил"));
// console.log(_data_.Person);
// window.app = app;
})();
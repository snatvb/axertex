module.exports = function (grunt) {
 
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', ['browserify', 'watch']);
 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify: {
          main: {
            src: 'js/app/app.js',
            dest: 'js/app.js',
            bundleOptions: {
                debug: true
            }
          }
        },
        watch: {
          files: ['js/app/*', 'js/app/core/*', 'js/app/core/senders/*'],
          tasks: ['default']
        }
    })
};